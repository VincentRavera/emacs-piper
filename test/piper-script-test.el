;;; PIPER-TEST-SCRIPT --- Unit tests for functions in `piper-script.el'
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created: 13 October 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(defvar piper-script-test-path
  (f-dirname (f-this-file)))

(defvar piper-script-code-path
  (f-parent piper-script-test-path))

(require 'piper-script (f-expand "piper-script.el" piper-script-code-path))


(ert-deftest piper-script-test-string-variables ()
  (should (s-starts-with? (getenv "HOME")
                          (piper-script
                           (echo "$HOME"))))

  (should (s-starts-with? (getenv "HOME")
                          (piper-script
                           (echo "${HOME}")))))

(ert-deftest piper-script-external-commands ()
  (should (s-starts-with? "SECOND.TXT"
                 (piper-script
                  ($ "ls" "-1" "test/test-data/")
                  (| "grep" "second")
                  (| "tr [a-z] [A-Z]")))))

(ert-deftest piper-script-test-setenv ()
  (piper-script-setenv-variable "piper-script-test-foo" "bar")
  (should (equal "bar" (getenv "piper-script-test-foo")))
  (piper-script-setenv-variable 'piper-script-test-far "out")
  (should (equal "out" (getenv "piper-script-test-far")))
  (should (equal "world\n"
                 (piper-script
                  (setenv 'piper-script-test-hello "world")
                  (echo "${piper-script-test-hello}")))))

(ert-deftest piper-script-test-get-files ()
  (let* ((results (piper-script
                   ;; This test will only work if ran in the project root directory...
                   (echo "test/test-data/*.txt")))
         (files   (->> results
                     s-trim
                     (s-split (rx blank))))
         (names   (-map 'f-filename files))
         (expects '("first.txt" "second.txt" "third.txt" "fourth.txt")))

    ;; The expansion of the files should end up being absolute:
    (should (-all? 'f-absolute? files))
    ;; It should also find all the files in that directory:
    (should (--all? (member it names) expects))))

(ert-deftest piper-script-test-get-no-files ()
  ;; First, make sure we don't get odd expansions when no files are given:
  (should (equal (piper-script-get-files "") ""))
  (should (equal (piper-script-get-files "hello") "hello"))
  (should (equal (piper-script-get-files "hello world") "hello world"))

  ;; Wildcards that don't match anything should just return their original string...
  (should (equal (piper-script-get-files "test/test-data/*.foobar")
                 "test/test-data/*.foobar"))

  ;; A directory should return an absolute value:
  (should (f-absolute? (piper-script-get-files "test/test-data")))

  ;; Detecting home directories is a little more difficult to match
  (should (s-starts-with? (getenv "HOME") (first (piper-script-get-files "~/*"))))
  (should (equal (piper-script-get-files "foo~/bar") "foo~/bar")))

(ert-deftest piper-script-test-get-files-variables ()
  (let ((foo "bar"))
    (should (equal "hello bar" (piper-script-get-files "hello $foo")))
    (should (equal "hello bar" (piper-script-get-files "hello ${foo}")))
    (should (s-starts-with? (getenv "HOME")
                            (piper-script-get-files "${HOME}")))
    ;; Non-existent variables should return an empty string:
    (should (equal "hello, " (piper-script-get-files "hello, $fred")))))

(ert-deftest piper-script-test-echo ()
  (should (equal "" (s-trim
                     (piper-script (echo)))))
  (should (equal "" (s-trim
                     (piper-script (echo "   ")))))
  (should (equal "" (s-trim
                     (piper-script (echo "")))))
  (should (equal "hello world"
                 (s-trim (piper-script
                          (echo "hello world"))))))

(ert-deftest piper-script-test-shell ()
  (should (equal "\n" (piper-script ($ "echo"))))
  (should (equal "hello world\n"
                 (piper-script
                  ($ "echo hello world"))))
  (should (equal "hello world\n"
                 (piper-script
                  ($ "echo" "hello" "world"))))
  ;; Let's make sure we can honor spaces in parameters
  ;; In this case, if -n is given to echo as a single parameter,
  ;; then it will be affect how echo responds, but without a space
  ;; in the command, the shell honors the parameters we give it,
  ;; giving it a single parameter.
  (should (equal "-n hello world\n"
                 (piper-script
                  ($ "echo" "-n hello world")))))

(ert-deftest piper-script-test-loop ()
  (should (equal "abc-hello-xyz\nabc-world-xyz\n"
                 (piper-script
                  (echo "hello")
                  (echo "world")
                  (for (line (read-all-lines))
                       (echo "abc-${line}-xyz"))))))

(ert-deftest piper-script-test-shell-command-to-list ()
  (let ((results (shell-command-to-list "ls -1")))
    (should (listp results))
    (should (> (length results) 3))
    (should (= 0 (length (--filter (s-blank-str? it) results))))))

(ert-deftest piper-script-test-shell-command-to-list-empty ()
  (let ((results (shell-command-to-list "ls -1 *.blah 2>/dev/null")))
    (should (null results))))

(defun file-contents (filename)
  "Returns the contents of FILENAME."
  (let (contents)
    (find-file filename)
    (setq contents (buffer-substring-no-properties (point-min) (point-max)))
    (kill-buffer)
    contents))

(ert-deftest piper-script-test-file-add ()
  (save-excursion
    (let ((tf (make-temp-file "emacs-testing")))
      (with-temp-file tf
        (insert "line one\nline two\nline three\n"))

      (piper-script-file-add-or-update tf "line four")

      (let ((expected "line one\nline two\nline three\nline four")
            (results (file-contents tf)))
        (should (equal expected results))))))

(ert-deftest piper-script-test-file-update ()
  (save-excursion
    (let ((tf (make-temp-file "emacs-testing")))
      (with-temp-file tf
        (insert "line one\nline two\nline three\n"))

      (piper-script-file-add-or-update tf "line four" "two")

      (let ((expected "line one\nline four\nline three\n")
            (results (file-contents tf)))
        (should (equal expected results))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; piper-test-script.el ends here
