﻿;;; PIPER-SCRIPT --- DSL for creating Shell-like Scripts
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created:  9 October 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;; A collection of functions helpful in attempting to translate shell
;; scripts into Elisp scripts.
;;
;; For instance, to translate the following shell script:
;;
;;      for IP in $(arp -a | grep 192.168 | sed 's/.*(\([0-9][0-9.]*\)).*/\1/')
;;      do
;;          ping -c 1 $IP
;;      done
;;
;; You could write:
;;
;;      (piper-script
;;         ($ "arp -a")
;;         (| "grep" "192.168")
;;         (| "sed" "s/.*(\\([0-9][0-9.]*\\)).*/\\1/")
;;         (dolist (ip (read-all-lines))
;;           ($ "ping" "-c 1" ip)))
;;
;; Or better yet, take advantage of Emacs functions:
;;
;;      (let ((line-with-ip (rx (*? any) "("
;;                              (group (one-or-more (any digit ".")))
;;                              ")" (* any))))
;;        (piper-script
;;         ($ "arp -a")
;;         (keep-lines "192.168")
;;         (replace-regexp line-with-ip "\\1")
;;         (dolist (ip (read-all-lines t))
;;           ($ "ping" "-c 1" ip))))
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(require 'cl)
(require 'dash)
(require 'em-glob)
(require 'f)
(require 's)

(require 'source-environment)

(defmacro piper-script (&rest forms)
  "Runs the FORMS in a shell-like DSL, for instance:

(let ((dot-files-src \"/tmp/dot-files\"))
  (piper-script
    (ln-s \"${dot-files-src}/bash_profile\" \"${HOME}/.bash_profile\")
    (ln-s \"${dot-files-src}/vimrc\" \"${HOME}/.vimrc\")

    (echo \"Installing some applications...\")
    (sudo \"snap install ripgrep --classic\"
          \"snap install telegram-desktop\"
          \"apt install -y i3-wm fonts-font-awesome\")))"
  (cons 'with-temp-buffer
        (-snoc (-tree-map #'piper--script-transform forms)
               '(buffer-string))))

(defun piper-script-sh (command-string)
  "Wrapper around the `shell-command' that interpolates the
variables and file expansions in the COMMAND-STRING before
executing it. This is the same effect as:

    (piper-script
       ($ \"ls /tmp\"))"
  (shell-command-to-string (piper-script-get-files command-string)))

(defun piper--script-transform (element)
  "Helper for the `piper-script' macro that converts certain
forms (and strings) into a new function call."
  (cond
   ((stringp element) `(piper-script-get-files ,element))

   ((eq element 'sh) 'piper-script-shell) ((eq element 'shell) 'piper-script-shell)
   ((eq element '$) 'piper-script-shell) ((eq element '|) 'piper-script-shell)

   ((eq element 'sudo) 'piper-script-sudo)
   ((eq element 'echo) 'piper-script-echo)
   ((eq element 'setenv)  'piper-script-setenv-variable)
   ((eq element 'source)  'source-environment)
   ((eq element 'ifsh) 'piper-script-shell-if)
   ((eq element 'for) 'piper-script-loop)

   ((eq element 'touch) 'f-touch)    ; Remove the `f' from the namespace?
   ((eq element 'cp) 'f-copy)        ; Saving four characters?
   ((eq element 'mv) 'f-move)        ; Easy enough to remember?
   ((eq element 'rm) 'f-delete)
   ((eq element 'ln-s) 'f-symlink)
   ((eq element 'mkdir) 'f-mkdir)

   ((eq element 'grep) 'piper-script-grep)              ((eq element 'keep-lines) 'piper-script-grep)
   ((eq element 'grep-v) 'piper-script-grep-v)          ((eq element 'flush-lines) 'piper-script-grep-v)
   ((eq element 'replace) 'piper-script-replace-regexp) ((eq element 'replace-regexp) 'piper-script-replace-regexp)

   ((eq element 'sort) 'piper-script-sort)
   ((eq element 'sort-r) 'piper-script-sort-r)
   ((eq element 'uniq) 'piper-script-uniq)

   ((eq element 'cat) 'insert-file-contents)
   ((eq element 'write-into) 'piper-script-write-into)
   ((eq element 'to-clipboard) 'piper-script-to-clipboard)

   ((eq element 'read-all-lines) 'piper-script-lines-to-list)
   ;; ... I'll be adding more!
   (t element)))


(defun piper-file-expand (s)
  "Returns result of `file-expand-wildcards' if non-nil, otherwise, returns S."
  (or (f-glob s) s))

(defun piper--script-getvar (var-name)
  "Return value of a variable or environment variable specified
by VAR-NAME. It the variable is not found, this returns an empty string."
  (condition-case nil
      (or (getenv var-name) (eval (read var-name)))
    (void-variable "")))

(defun piper--script-replvar (matched)
  "Calls `piper--script-getvar' with the first matched group in MATCHED."
  (piper--script-getvar (match-string 1 matched)))

(defun piper-script-get-files (str)
  "Return list of files that match glob patterns, PATH.
Also expands the string similar to the shell, including expanding
variable patterns that look like either $HOME and ${HOME} with
values from either the environment or from expand Lisp
variables (defined with `defvariable' or `setq')."
  (if (s-blank-str? str)
      ""
    (let* ((simple-var (rx "$" (group (1+ (in alnum "_")))))        ; e.g. $HOME
           (braced-var (rx "${" (group (1+ (not (any "}")))) "}"))  ; e.g. ${HOME}
           (expand-var (--> str
                          (replace-regexp-in-string simple-var #'piper--script-replvar it t)
                          (replace-regexp-in-string braced-var #'piper--script-replvar it t)
                          (piper-file-expand it))))
      (if (= 1 (length expand-var))
          (first expand-var)
        expand-var))))

;; --------------------------------------------
;;  HELPING and SUPPORTING COMMAND FUNCTIONS
;; --------------------------------------------

(defun piper-script-shell (command &rest parameters)
  "Simple wrapper around `shell-command' that concatenates all the parameters."

  ;; Either give a shell command with its parameters as a list (in which case,
  ;; we honor the separation of the command and its arguments), or if the shell
  ;; command is a string (at least it has a space), break entire list on spaces:
  (when (s-contains? " " command)
    (let* ((parts (->> (cons command parameters)
                     (--map (if (stringp it) (s-split (rx (1+ blank)) it t) it))
                     -flatten)))
      (setq command (first parts))
      (setq parameters (rest parts))))

  (message "piper-script: Executing: %s %s" command parameters)
  ;; If there is no text in the temporary buffer to send to the command,
  ;; don't bother sending anything and call `process-file',
  ;; Otherwise, we'll process the buffer as a region:
  (if (= (point-max) 1)
      (apply 'process-file command nil t nil parameters)
    (apply 'process-file-region (point-min) (point-max) command t t nil parameters)))

(defun process-file-region (start end program
                                  &optional delete destination display
                                  &rest args)
  "Similiar to `call-process-region', but attempts to work with remote Tramp references.
It does this by first writing out the region to a temporary file
on the remote system, and converting all ARGUMENTS through
`file-local-name'."
  (let ((relative-args (--map (file-relative-name it) args))
        (remote-file (make-temp-file "piper")))
    (write-region start end remote-file)
    (when delete
      (delete-region start end))
    (apply 'process-file program remote-file destination display relative-args)))

(defun piper-script-echo (&rest stuff)
  "Behaves like the `echo' shell command and inserts the
parameters into the buffer."
  (insert (s-join " " (-flatten stuff)))
  (insert "\n"))

(defun piper-script-setenv-variable (var value)
  "Simple way to set environment variables."
  (setq value (format "%s" value))
  (if (symbolp var)
      (setenv (symbol-name var) value)
    (setenv var value)))

(defun piper-script-shell-if (command &optional expected-results)
  "Runs the COMMAND through the shell, and returns `t' if the
results from standard out matches the value in EXPECTED-RESULTS."
  (-let* ((parts (s-split (rx blank) command))
          ((ecode results) (apply #'shell-command-with-exit-code parts))
          (trimmed (s-trim results)))
    (if expected-results
        (equal trimmed expected-results)
      (eq ecode 0))))

(cl-defmacro piper-script-loop ((var sequence) &rest forms)
  "Iterates over SEQUENCE assigning VAR to each element, and
executing each of the FORMS in turn. This assumes that the forms
write into the current buffer.

This is very similar to `dolist', but puts the contents of SEQUENCE
in a closure, and then erases the current buffer before executing FORMS."
  (list 'let
        `((lst (-remove 's-blank-str? ,sequence)))
        '(erase-buffer)
        `(-map (lambda (,var) ,(cons 'progn forms)) lst)))

(defun piper-script-ubuntu? ()
  "Returns `t' if called on an Ubuntu system."
  (piper-script-shell-if "lsb_release -is" "Ubuntu"))

(defun piper-script-mac? ()
  "Returns `t' if called on an Apple Mac system."
  (piper-script-shell-if "which osascript"))

(defun piper-script--add-sudo-to-tramp (&optional directory)
  "Returns `default-directory' with an added `sudo' added to
the URL file reference. For instance:
  /etc/passwd → /sudo:localhost:/etc/passwd
And:
  /ssh:some-host:/etc/passwd → /ssh:some-host|sudo:localhost:/etc/passwd "

  (let* ((def-dir (or directory default-directory))
         (remote-tramp-re (rx (group (1+ (any print))) ":" (group (1+ (not (any ":")))))))

    (if (string-match remote-tramp-re def-dir)
        (concat (match-string 1 def-dir) "|sudo:localhost:" (match-string 2 def-dir))
      (concat "/sudo:localhost:" def-dir))))

(defmacro piper-script-sudo (&rest forms)
  "Runs the FORMS using `default-directory' set to a
/sudo:localhost Tramp reference."
  (cons 'let
        (cons '((default-directory (piper-script--add-sudo-to-tramp)))
              forms)))

(defun shell-command-to-list (command)
  "Runs COMMAND in the shell, but returns a list of lines from the resulting output.
While this is interactive, I can't imagine using it that way."
  (interactive "sCommand: ")
  (->> command
     shell-command-to-string
     s-lines
     (--map (s-trim it))
     (--remove (s-blank-str? it))))

(defun piper-script-file-add-or-update (file new-line &optional matcher)
  "Many a configuration FILE needs updating, and unless the
contents of NEW-LINE are already in the file, we add NEW-LINE to
end of the file. Sometimes, the line just needs updating, so if
we find a line that matches the regular expression, MATCHER, we
replace that line. I seem to run into this often.

If we modify the file, we return non-nil."
  (save-excursion
    (find-file file)

    ;; Unless `new-line' already in the file, we don't need to do a thing...
    (goto-char (point-min))
    (unless (search-forward new-line nil t)

      (if (and matcher (re-search-forward matcher nil t))
          (progn
            (beginning-of-line)
            (kill-line))
        (goto-char (point-max)))

      (insert new-line)
      (save-buffer)
      ;; If we modified the file, then we return t...
      t)))

(provide 'piper-script)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; piper-script.el ends here
